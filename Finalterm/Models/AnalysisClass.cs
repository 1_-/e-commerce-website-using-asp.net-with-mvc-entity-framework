﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Models
{
    public class AnalysisClass
    {
        public double Jan { get; set; }
        public double feb { get; set; }
        public double march { get; set; }
        public double april { get; set; }
        public double may { get; set; }
        public double jun { get; set; }
        public double july { get; set; }
        public double Aug { get; set; }
        public double Sept { get; set; }
        public double Nov { get; set; }
        public double Dec { get; set; }
        public double Oct { get; set; }
        public List<Profitcatagory> catagory { get; set; }
        public string sJSONResponse { get; set; }

        public List<Product> products { get; set; }

    }
}